FROM sutty/sdk:latest
MAINTAINER "f <f@sutty.nl>"

RUN apk update
RUN cp /home/builder/.abuild/*.pub /etc/apk/keys/

WORKDIR /home/builder
COPY ./APKBUILD .
COPY ./monitrc .
COPY ./monit.initd .
COPY ./fork.patch .
RUN chown -R builder .

USER builder
RUN abuild checksum
RUN abuild -r

USER root
